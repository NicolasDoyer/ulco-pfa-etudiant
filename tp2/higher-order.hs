import Data.List

-- threshList
threshList s = map (min s)

-- selectList
selectList s = filter (<s)

-- maxList
maxList xs = foldr1 max xs

main :: IO ()
main = do
    print $ threshList 3 [1..5::Int]
    print $ selectList 3 [1..5::Int]


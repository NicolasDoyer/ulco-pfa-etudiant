
-- safeTailString 
safeTailString :: String -> String
safeTailString "" = ""
safeTailString (_:xs) = xs

-- safeHeadString 
safeHeadString :: String -> Maybe Char
safeHeadString "" = Nothing
safeHeadString (x:_) = Just x

-- safeTail 
safeTail :: [a] -> [a]
safeTail [] = []
safeTail (_:xs) = xs

safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x


-- safeHead 

main :: IO ()
main = do
     putStrLn $ safeHeadString "testatos"
     putStrLn $ safeTailString ""
     



-- TODO User
data User = User
    { 
        lastname :: String,
        firstname :: String,
        age :: Int
    } deriving (Show)

showUser :: User -> String
showUser user = show user

showUser2 :: User -> String
showUser2 (User f l a) = f ++ " " ++ l ++ " " ++ show a 

incAge :: User -> User
incAge u = u { age = age u + 1}

main :: IO ()
main = do
    print $ showUser (User "Toto" "Tata" 10)
    print $ showUser2 (User "Toto" "Tata" 10)
    print $ showUser ( incAge (User "Toto" "Tata" 10) )


import Data.List (foldl')

-- TODO Abr
data Abr a = Empty | Node a (Abr a) (Abr a) deriving (Show)

-- insererAbr 
insererAbr :: Ord a => a -> Abr a -> Abr a
insererAbr x Empty = Node x Empty Empty
insererAbr x (Node a left right)
    | x < a = Node a (insererAbr x left) right
    | otherwise = Node a left (insererAbr x right)

-- listToAbr
listToAbr list = buildAbr list Empty
buildAbr [] _ = Empty
buildAbr (x:xs) oldTree = insererAbr x nextTree
    where nextTree = buildAbr xs oldTree 

-- abrToList
abrToList Empty = []
abrToList (Node a left right) = abrToList left ++ [a] ++ abrToList right

main :: IO ()
main = putStrLn "TODO"



-- TODO Jour
data Jour = Monday | Tuesday | Thursday | Wednesday | Friday | Saturday | Sunday

estWeekend :: Jour -> Bool
estWeekend Saturday = True
estWeekend Sunday = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables days = length (filter estWeekend days)

main :: IO ()
main = do
    print $ estWeekend Friday
    print $ estWeekend Saturday
    print $ compterOuvrables [Monday, Tuesday, Sunday, Friday, Saturday]
    print $ compterOuvrables [Monday, Tuesday, Friday]



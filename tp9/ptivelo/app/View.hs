{-# LANGUAGE OverloadedStrings #-}

module View where

import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import Lucid
import TextShow (showtl)

import Model

homePage :: [Rider] -> L.Text
homePage riders = mkPage "Homepage" $ do
    h1_ "Homepage"
    ul_ $ do mapM_ mkRider riders

bootstrap :: T.Text
bootstrap = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"

mkPage :: L.Text -> Html () -> L.Text
mkPage myTitle myHtml = renderText $ do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [ name_ "viewport"
                  , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            title_ $ toHtml myTitle
        body_ $ div_ [class_ "container"] myHtml

mkRider (Rider name images) = do
    li_ $ do
        p_ name
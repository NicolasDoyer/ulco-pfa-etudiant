data Tree a = Leaf | Node a (Tree a) (Tree a)

instance (Show a) => Show (Tree a) where
    show Leaf = "_"
    show (Node x l r) = "(" ++ show x ++ show l ++ show r ++ ")"

instance Foldable Tree where
    foldMap f Lead = mempty
    foldMap f (Node x l r) = f x 

mytree1 :: Tree Int
mytree1 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))

main = do
    print mytree1
    -- print mytree2
    -- print $ sum mytree1
    -- print $ maximum mytree1


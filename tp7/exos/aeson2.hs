{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: "birthyear"
        <*> v .: "speakenglish"

main :: IO ()
main = do
    res0 <- eitherDecodeFileStrict "aeson-test1.json"
    print (res0 :: Either String Person)

    res1 <- eitherDecodeFileStrict "aeson-test2.json"
    print (res1 :: Either String [Person])

    res2 <- eitherDecodeFileStrict "aeson-test3.json"
    print (res2 :: Either String [Person])


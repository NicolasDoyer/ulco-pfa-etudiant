import qualified Data.Text.IO as T
import qualified Data.Text.Encoding as T
import qualified Data.ByteString.Char8 as C

main :: IO ()
main = do
    content <- C.readFile "text0.hs"
    T.putStrLn $ T.decodeUtf8 content


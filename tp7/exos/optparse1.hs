import Options.Applicative

data Args = Args
    { hello      :: String
    , file       :: String
    , val1       :: Int
    , val2       :: Double
    } deriving (Show)

argsP :: Parser Args
argsP = Args
      <$> strOption
          ( long "hello"
         <> help "Target for the greeting"
         <> metavar "TARGET")
      <*> strArgument
         (help "Output file"
         <> metavar "FILE")
      <*> option auto
          ( long "val1"
         <> help "Value 1"
         <> metavar "INT"
         <> value 1)
      <*> option auto
          ( long "val2"
         <> help "Value 2"
         <> metavar "DOUBLE"
         <> value 1)

myinfo :: ParserInfo Args
myinfo = info (argsP <**> helper)
              (fullDesc <> header "This is my cool app!")

main :: IO ()
main = execParser myinfo >>= print


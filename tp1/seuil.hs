-- seuilInt
seuilInt :: Int -> Int -> Int -> Int
seuilInt lower higher x
    | x < lower = lower
    | x > higher = higher
    | otherwise = x

-- seuilTuple
seuilTuple :: (Int, Int) -> Int -> Int
seuilTuple (x0, x1) x = max x0 $ min x1 x

main :: IO ()
main = do
    print $ seuilInt 1 10 0
    print $ seuilTuple (1,10) 11
